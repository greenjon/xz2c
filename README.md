Upgrade to Android 10

Unlock bootloader:

    https://wiki.lineageos.org/devices/akari/install

    http://developer.sonymobile.com/unlockbootloader/unlock-yourboot-loader/

Enable VoLTE in stock ROM

Disable AVB2: fastboot --disable-verity --disable-verification flash vbmeta vbmeta.img

Install Lineage Recovery 
    https://download.lineageos.org/xz2c

Copy a to b partition

Download Lineage and Magisk and flash from ADB
    https://download.lineageos.org/xz2c


Deodex ROM

Patch for signature spoofing

Install MicroG

Install Camera
    https://androidfilehost.com/?w=files&flid=312775
